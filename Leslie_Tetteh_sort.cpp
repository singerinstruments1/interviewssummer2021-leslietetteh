#include <iostream> 
#include <set>  
#include <functional> 
#include <fstream>
#include <chrono>
#include <string>

#define SEC_MILLISEC_CONVERSION 1000

using namespace std;

int main(){
    string newString = "";
    multiset<char, less_equal <char>> newSet; //multisets allow you to use multiples of the same value, will still sort values
     
    cout << "Please enter a string or filename: ";
    getline(cin, newString);

    if (newString.substr(0, 5) == "file=") //sorting function if input is a file
    {
        ifstream inFile;
        ofstream outFile;
        char ch;
        newString = newString.replace(0,5, ""); //removes "file=" from beginning of string
        auto start = std::chrono::system_clock::now();
        
        inFile.open(newString, ios::binary);

        if (!inFile.is_open())
        {
            cout << "Could not open this file\n";
     	    return 1;
        }
        inFile.seekg(ios::beg);

        while (!inFile.eof())  //sorting algorithm
        {
            inFile >> ch;
	        if ((ch >= '!') && (ch <= '~'))  // if > 0x20 && < 0x7f
                newSet.insert(ch);  //inserting into multiset makes use of binary tree structure to sort, see tree_C++_explanation.txt
        }
        inFile.close();

        auto fin = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = (fin - start);
        cout << "Elapsed milliseconds = " << elapsed_seconds.count() * SEC_MILLISEC_CONVERSION << "ms\n";

        string fileName = "sortedCharOutput.txt";
        outFile.open(fileName);

        for (auto it : newSet)
        {
            outFile << it;
        }

        outFile.close();

        inFile.open(fileName, ios::binary|ios::ate);
        long end = inFile.tellg();
        cout << "Filename = " << fileName << " size = " << end - 0 << " bytes\n";

        outFile.close();
    }
    else //sorting function if input is a string
    {
        auto start = std::chrono::system_clock::now();
        for (int i = 0; i < newString.length(); i++)
        {
    	    if ((newString.at(i) >= '!') && (newString.at(i) <= '~')) // if > 0x20 && < 0x7f
	            newSet.insert(newString.at(i)); //inserting into multiset makes use of binary tree structure to sort, see tree_C++_explanation.txt
        }
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = (end - start);
        cout << "Elapsed milliseconds = " << elapsed_seconds.count() * SEC_MILLISEC_CONVERSION << "ms\n";

        for (auto it : newSet)
        {
            cout << it;
        }
        cout << endl;
    }

    return 0;
}